const balanced = (word) => {
    const alphabet = 'abcdefghijklmnopqrstuvwxyz';
    let leftSum = 0;
    let rightSum = 0;

    // check if the word is even
    if (word.length % 2 === 0) {
      for (let i = 0; i < word.length / 2; i++) {
        // loop the word from left to right
        leftSum += alphabet.indexOf(word.charAt(i)) + 1;
        // loop the word from right to left
        rightSum += alphabet.indexOf(word.charAt(word.length - 1 - i)) + 1;
      }
    } else {
        // check if the word is odd
      for (let i = 0; i < Math.floor(word.length / 2); i++) {
        leftSum += alphabet.indexOf(word.charAt(i)) + 1;
        rightSum += alphabet.indexOf(word.charAt(word.length - 1 - i)) + 1;
      }
    }
    
    return leftSum === rightSum;
  };
  
  console.log(balanced("zips")); 
  console.log(balanced("brake")); 
  console.log(balanced("at")); 
  console.log(balanced("abstract")); 
  


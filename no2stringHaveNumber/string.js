
const  numInStr = (arr) => {
    return arr.filter(str => {
      for (let i = 0; i < str.length; i++) {
        if (!isNaN(Number(str[i]))) {
          return true;
        }
      } 
      return false;
    });
  }
  
  const result = numInStr(["1a", "a", "2b", "b"]);
  console.log(result);
  
const argvs = process.argv
const argv = argvs.slice(2)
const operation = argv[0]
const operator1 = parseInt(argv[1])
const operator2 = parseInt(argv[2])


if (operation === 'add') {
    console.log(operation + ' is ' + (operator1 + operator2));
  } else if (operation === 'subtract') {
    console.log(operation + ' is ' + (operator1 - operator2));
  } else if (operation === 'devide') {
    if (operator2 !== 0) {
      console.log(operation + ' is ' + (operator1 / operator2));
    } else {
      console.log(operation + ' is' + (' undefined'));
    }
  } else if (operation === 'multiply') {
    console.log(operation + ' is '  + (operator1 * operator2));
  } else {
    console.log('Invalid operation');
  }

//   How to use:
// node calculator.js add <number> <number> // Addition of two numbers
// node calculator.js subtract <number> <number> // Subtraction of two numbers
// node calculator.js devide <number> <number> // devide of two numbers
// node calculator.js multiply <number> <number> // multiply of two numbers/


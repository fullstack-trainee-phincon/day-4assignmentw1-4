const leftSlide = (array) => {
    const sortedArray = array.sort((a,b) => a - b)
    const resultArrray = []

    let currentNumber = sortedArray[0]
    let sum = currentNumber

    for  (let i=0; i < sortedArray.length; i++) {
        if (sortedArray[i] === currentNumber) {
            sum += sortedArray[i]
        } else {
            resultArrray.push(sum);
            currentNumber = sortedArray[i];
            sum = currentNumber
        }
    }
    resultArrray.push(sum)

    return resultArrray
}

console.log(leftSlide([0, 2, 2, 8, 8, 8]))
console.log(leftSlide([0, 2, 0, 2, 4]))
